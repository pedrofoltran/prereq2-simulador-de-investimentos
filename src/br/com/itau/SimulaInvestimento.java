package br.com.itau;

public class SimulaInvestimento {

    public static double simular(Produto produto){
        return produto.getValorInvestido() + produto.getTempoInvestido()*produto.getValorInvestido()*produto.getTaxaRentabilidade().getValorJuros();
    }
}
