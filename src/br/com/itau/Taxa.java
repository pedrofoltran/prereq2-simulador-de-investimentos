package br.com.itau;

public class Taxa {
    private double valorJuros;

    public Taxa(double valorJuros){
        this.valorJuros = valorJuros;
    }

    public double getValorJuros() {
        return valorJuros;
    }
}
