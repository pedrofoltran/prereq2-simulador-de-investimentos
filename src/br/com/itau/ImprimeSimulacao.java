package br.com.itau;

public class ImprimeSimulacao {
    public static void imprimirSimulacao(double valorPosInvestimento, Produto produtoInvestido){
        System.out.println("**  Simulação para produto **");
        System.out.print("Valor investido: " + produtoInvestido.getValorInvestido());
        System.out.print(" em " + produtoInvestido.getTempoInvestido() + " meses");
        System.out.println(" com uma taxa de " + String.format("%.2f", produtoInvestido.getTaxaRentabilidade().getValorJuros()*100) + "% ao mes");
        System.out.print("Rendeu um total de R$"+ valorPosInvestimento+" após final do período");

    }
}
