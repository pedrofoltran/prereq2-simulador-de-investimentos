package br.com.itau;

public class Produto {
    private double valorInvestido;
    private int tempoInvestido;
    private Taxa taxaRentabilidade;

    public Produto(double valorInvestido, int tempoInvestido, Taxa taxa){
        this.valorInvestido = valorInvestido;
        this.tempoInvestido = tempoInvestido;
        this.taxaRentabilidade = taxa;
    }

    public double getValorInvestido() {
        return valorInvestido;
    }

    public int getTempoInvestido() {
        return tempoInvestido;
    }

    public Taxa getTaxaRentabilidade() {
        return taxaRentabilidade;
    }
}
