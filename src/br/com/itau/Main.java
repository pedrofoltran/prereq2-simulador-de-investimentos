package br.com.itau;

public class Main {

    public static void main(String[] args) {
        Taxa taxaProduto = new Taxa(0.007);
        double valorPosInvestimento;

        Produto novoInvestimento = new Produto(CapturaInvestimento.capturaValor(), CapturaInvestimento.capturaMeses(), taxaProduto);
        valorPosInvestimento = SimulaInvestimento.simular(novoInvestimento);
        ImprimeSimulacao.imprimirSimulacao(valorPosInvestimento, novoInvestimento);

    }
}
