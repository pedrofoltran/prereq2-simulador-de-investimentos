package br.com.itau;

import java.util.Scanner;

public class CapturaInvestimento {

    public static double capturaValor(){
        Scanner captura = new Scanner(System.in);

        System.out.println("Digite o valor do investimento:");
        return captura.nextDouble();
    }

    public static int capturaMeses(){
        Scanner captura = new Scanner(System.in);

        System.out.println("Digite a quantidade de Meses:");
        return captura.nextInt();
    }


}
